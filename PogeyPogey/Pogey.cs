﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;

namespace PogeyPogey
{

    class PogeyC
    {
        private static int MAX = 5;
        public int Pogeys;
        public string OnPogey;
        public string DoPogey;

        public PogeyC(string on, string dop)
        {
            Pogeys = 1;
            OnPogey = on;
            DoPogey = dop;
        }

        public string DoPogeys()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Pogeys; i++)
            {
                sb.Append(DoPogey + " ");
            }

            Pogeys += 1;
            if (Pogeys > MAX)
            {
                Pogeys = 1;
            }
            return sb.ToString().TrimEnd();
        }

        public override string ToString()
        {
            return $"{OnPogey}[{Pogeys}]=>{DoPogey}[{MAX}]";
        }
    }
    public class Pogey
    {
        private readonly string C;

        private readonly PogeyC[] Pogeys;

        private readonly TwitchClient _cl;

        public Pogey()
        {
            string u;
            string t;
            string c;

            using (var sr = new StreamReader(File.OpenRead("creds.txt")))
            {
                u = sr.ReadLine()?.Trim() ?? "";
                t = sr.ReadLine()?.Trim() ?? "";
                c = sr.ReadLine()?.Trim() ?? "";
            }

            if (u == "" || t == "" || c == "")
            {
                throw new IOException("Missing creds");
            }

            C = c;

            using (var sr = new StreamReader(File.OpenRead("pogey.txt")))
            {
                var pg = sr.ReadToEnd();
                var pl = new List<PogeyC>();
                foreach (var s in pg.Split('\n'))
                {
                    var q = s.Trim();
                    var sp = q.Split(':');
                    if (sp.Length == 2)
                    {
                        pl.Add(new PogeyC(sp[0], sp[1]));
                    }
                }

                Pogeys = pl.ToArray();
            }

            _cl = new TwitchClient();
            _cl.OnMessageReceived += ClOnMessageReceived;
            _cl.Initialize(new ConnectionCredentials(u, t), c);
            _cl.Connect();
            
            Console.WriteLine($"{u} => {c}");
            Console.WriteLine($"POGEYS => [{Pogeys.Select((v) => v.ToString()).Aggregate((x, y) => $"{x}, {y}")}]");
        }

        private void ClOnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            Console.WriteLine($"{DateTime.Now:O} [{e.ChatMessage.Username.PadRight(15)} @ {e.ChatMessage.Channel.PadLeft(15)}] > {e.ChatMessage.Message}");

            switch (e.ChatMessage.Username)
            {
                case "heapunderflow":
                    return;
                case "xTeaTurtle":
                    _cl.SendMessage(C, "no");
                    return;
            }

            foreach (var pog in Pogeys)
            {
                // Console.WriteLine(pog);
                if (!e.ChatMessage.Message.Contains(pog.OnPogey)) continue;
                Console.WriteLine(
                    $"{DateTime.Now:O} {pog.OnPogey} Triggered > {e.ChatMessage.Username} in {e.ChatMessage.Channel} for {pog.Pogeys} {pog.OnPogey}'s");
                _cl.SendMessage(C, pog.DoPogeys());
                return;
            }
        }
    }
}